## Prerequisites
1. [NodeJS](https://nodejs.org/dist/v12.18.3/node-v12.18.3-x64.msi)
2. [WampServer](https://www.wampserver.com/en/download-wampserver-64bits/)

# Database setup
1. Create database `nfc`
2. Import `nfc.sql` into `nfc`

# Apache setup libraries for mapping subdomain (\wamp64\bin\apache\apache2.4.41\conf\httpd.conf)
Your must remove `#` for mod_proxy_http and mod_proxy

# Apache setup mapping subdomain (\wamp64\bin\apache\apache2.4.41\conf\extra\httpd-vhosts.conf)
You must change do kpongsukjai.com to be your domain from example below

<VirtualHost *:80><br/>
	ServerName kpongsukjai.com<br/>
	ProxyPass / http://localhost:3002/<br/>
	ProxyPassReverse / http://localhost:3002/<br/>
</VirtualHost><br/>
<VirtualHost *:80><br/>
	ServerName nfc-manager.kpongsukjai.com<br/>
	ProxyPass / http://localhost:3000/<br/>
	ProxyPassReverse / http://localhost:3000/<br/>
</VirtualHost><br/>
<VirtualHost *:80><br/>
	ServerName nfc-server.kpongsukjai.com<br/>
	ProxyPass / http://localhost:3001/<br/>
	ProxyPassReverse / http://localhost:3001/<br/>
</VirtualHost>


## Setup for development
1. npm install
2. change mysql credential in `services/mysql.js` (constructor)
2. npm start (for development please use nodemon `start instead` of `npm istart`)