const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs')
const mysql = require('./services/mysql/mysql')
const rimraf = require('rimraf')
const md5 = require('md5')
const app = new express()
const port = 3001
const TABLE_SESSION = 'session'
const TABLE_USERS = 'users'
const DATABASE_EVENT_PATH = './database/events'


app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }))
app.use(bodyParser.json({ limit: '50mb' }))
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Methods", "GET, POST")
    res.header("Access-Control-Allow-Headers", "*")
    next()
})


app.get('/session', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let user = await getUserInfo(token)
            if (user) {
                res.json({
                    fullName: user.full_name,
                    role: user.role
                })
            } else {
                res.json({
                    error: 'session_expired',
                    error_description: 'Session is expired.'
                })
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})


app.post('/login', async (req, res) => {
    let { username, password } = req.body
    password = md5(password)
    let users = await mysql.select(TABLE_USERS)
    for (let i in users) {
        if (username === users[i].user_name && users[i].password === password) {
            const randomToken = md5(users[i].user_name) + '' + md5((new Date().getTime()).toString()) + md5(Math.random().toString())

            await mysql.delete(TABLE_SESSION, {
                user_id: users[i].user_id
            })
            await mysql.insert(TABLE_SESSION, {
                token: randomToken,
                user_id: users[i].user_id
            })
            res.json({
                token: randomToken
            })
            return
        }
    }
    res.json({
        error: 'Incorrect username or password',
        error_description: 'Incorrect username or password'
    })
})

app.get('/accounts', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let user = await getUserInfo(token)
            if (user) {
                const accounts = await getAccounts('admin', true)
                res.json(accounts)
            } else {
                res.json({
                    error: 'session_expired',
                    error_description: 'Session is expired.'
                })
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/createAccount', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let user = await getUserInfo(token)
            if (user) {
                const form = req.body
                if (!form.username || form.username.length <= 3 || !form.password) {
                    res.json({
                        error: 'bad_request',
                        error_description: 'Bad request'
                    })
                    return
                }
                let accounts = await getAccounts()
                accounts = accounts.filter((account) => {
                    return account.user_name === form.username
                })
                if (accounts.length === 0) {
                    await mysql.insert(TABLE_USERS, {
                        user_name: form.username,
                        password: md5(form.password),
                        role: 'admin'
                    })
                    accounts = await getAccounts('admin', true)
                    res.json(accounts)
                    return
                } else {
                    res.json({
                        error: 'Username has already been taken.',
                        error_description: 'Username has already been taken.'
                    })
                }
            } else {
                res.json({
                    error: 'session_expired',
                    error_description: 'Session is expired.'
                })
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/deleteAccount', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let user = await getUserInfo(token)
            if (user) {
                const { user_id } = req.body
                await mysql.delete(TABLE_USERS, {
                    user_id: user_id
                })
                await mysql.delete(TABLE_SESSION, {
                    user_id: user_id
                })
                let accounts = await mysql.select(TABLE_USERS, {
                    role: 'admin'
                })
                accounts = accounts.map((account) => {
                    return {
                        user_id: account.user_id,
                        username: account.user_name,
                        role: account.role
                    }
                })
                res.json(accounts)
            } else {
                res.json({
                    error: 'session_expired',
                    error_description: 'Session is expired.'
                })
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/changeAccountPassword', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let user = await getUserInfo(token)
            if (user) {
                const { user_id, password } = req.body
                if (!password) {
                    res.json({
                        error: 'Bad request',
                        error_description: 'Bad request'
                    })
                } else {
                    await mysql.query("UPDATE users SET password=\"" + md5(password) + "\" WHERE user_id=" + user_id)
                    const accounts = getAccounts('admin', true)
                    res.json(accounts)
                }
            } else {
                res.json({
                    error: 'session_expired',
                    error_description: 'Session is expired.'
                })
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/editDeleteDay', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let user = await getUserInfo(token)
            if (user) {
                const { day, created_time } = req.body
                let event = getEvent(created_time)
                event.deleteDay = day
                updateEvent(event)
                res.json(event)
            } else {
                res.json({
                    error: 'session_expired',
                    error_description: 'Session is expired.'
                })
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/addAgeRanges', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let user = await getUserInfo(token)
            if (user) {
                const { created_time, value } = req.body
                let event = getEvent(created_time)
                if (!event.ageRanges) {
                    event.ageRanges = []
                }
                event.ageRanges.push({
                    id: new Date().getTime(),
                    text: value
                })
                updateEvent(event)
                res.json(event)
            } else {
                res.json({
                    error: 'session_expired',
                    error_description: 'Session is expired.'
                })
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/addConsent', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let user = await getUserInfo(token)
            if (user) {
                const { created_time, value } = req.body
                let event = getEvent(created_time)
                if (!event.consents) {
                    event.consents = []
                }
                if (event.consents) {
                    event.consents.push({
                        id: new Date().getTime(),
                        text: value
                    })
                }
                console.log(event)
                updateEvent(event)
                res.json(event)
            } else {
                res.json({
                    error: 'session_expired',
                    error_description: 'Session is expired.'
                })
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/deleteAgeRange', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let user = await getUserInfo(token)
            if (user) {
                const { created_time, id } = req.body
                let event = getEvent(created_time)
                if (!event.ageRanges) {
                    event.ageRanges = []
                }
                console.log(id)
                event.ageRanges = event.ageRanges.filter((ageRange) => {
                    return ageRange.id != id
                })
                updateEvent(event)
                res.json(event)
            } else {
                res.json({
                    error: 'session_expired',
                    error_description: 'Session is expired.'
                })
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/deleteConsent', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let user = await getUserInfo(token)
            if (user) {
                const { created_time, id } = req.body
                let event = getEvent(created_time)
                event.consents = event.consents.filter((consent) => {
                    return consent.id != id
                })
                updateEvent(event)
                res.json(event)
            } else {
                res.json({
                    error: 'session_expired',
                    error_description: 'Session is expired.'
                })
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/editEventUrl', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let user = await getUserInfo(token)
            if (user) {
                const { url, created_time } = req.body
                let event = getEvent(created_time)
                event.url = url
                updateEvent(event)
                res.json(event)
            } else {
                res.json({
                    error: 'session_expired',
                    error_description: 'Session is expired.'
                })
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/createEvent', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const { topic, description, size } = req.body
            const timestamp = new Date().getTime()
            const event = {
                topic: topic,
                description: description,
                created_time: timestamp,
                started_time: null,
                ended_time: null,
                status: 'Idle',
                deleteDay: 30,
                consents: [],
                ageRanges: []
            }
            const eventDirPath = DATABASE_EVENT_PATH + '/' + timestamp
            fs.mkdirSync(eventDirPath)
            fs.writeFileSync(eventDirPath + '/detail.json', JSON.stringify(event), 'utf8')
            fs.mkdirSync(eventDirPath + '/zones')
            fs.mkdirSync(eventDirPath + '/attendees')
            res.json({
                status: 'success'
            })
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/getEvents', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let events = getEvents(true)
            let status = []
            for (let i in req.body.status) {
                if (req.body.status[i]) {
                    status.push(i)
                }
            }
            if (status.length > 0) {
                events = events.filter((event) => {
                    return status.includes(event.status)
                })
            }
            res.json(events)
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})


app.post('/scan', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const { code, event, zone } = req.body
            const zone_ = zone
            const event_ = getEvent(event.created_time)
            if (!event_) {
                res.json({
                    error: 'event_is_invalid',
                    error_description: 'Event is invalid'
                })
                return
            }
            if (event_.status !== 'Active') {
                res.json({
                    error: 'event_is_not_active',
                    error_description: 'Event is not active'
                })
                return
            }
            let attendee = getAttendee(event.created_time, code.signature)
            for (let i in attendee.zones) {
                let attZone = attendee.zones[i].zone
                if (attZone === 'Not Zoned' || zone_ == attZone) {
                    let zone = getZone(event.created_time, zone_)

                    const timestamp = new Date().getTime()
                    const record = {
                        name: attendee.name,
                        mobileNo: attendee.mobileNo,
                        tagId: attendee.tagId,
                        created_time: timestamp,
                        actions: ''
                    }

                    let newCurrent = []
                    let added = false

                    // check this attendee is in zone or not, if yes remove attendee from current, if no add attendee into current
                    // and add attendee to records
                    for (let j in zone.current) {
                        let current = zone.current[j]
                        if (current.tagId != record.tagId) {
                            newCurrent.push(current)
                        } else {
                            added = true
                        }
                    }
                    if (!added) {
                        newCurrent.push(record)
                    }

                    zone.current = newCurrent
                    record.action = added ? 'Check-out' : 'Check-in'

                    if (record.action === 'Check-in') {
                        if (zone.size <= zone.current.length - 1) {
                            res.json({
                                status: 'unauthorized',
                                text: event.topic + ' is full'
                            })
                            return
                        }
                    }

                    zone.records.push(record)

                    updateZone(event.created_time, zone)

                    // add records for attendees
                    attendee.records.push({
                        created_time: timestamp,
                        event: event,
                        zone: {
                            name: zone.name,
                            created_time: zone.created_time
                        },
                        action: record.action
                    })
                    updateAttendee(event.created_time, attendee)
                    if (record.action === 'Check-out') {
                        res.json({
                            status: 'success',
                            text: '' + attendee.name + ' is exiting'
                        })
                    } else {
                        res.json({
                            status: 'success',
                            text: 'Welcome ' + attendee.name
                        })
                    }
                    return
                }
            }
            res.json({
                status: 'unauthorized',
                text: 'You are not granted in this zone'
            })
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/scanNative', async (req, res) => {
    try {
        const { code, event, zone } = req.body
        const zone_ = zone
        const event_ = getEvent(event.created_time)
        console.log(code)
        if (!event_) {
            res.json({
                error: 'event_is_invalid',
                error_description: 'Event is invalid'
            })
            return
        }
        if (event_.status !== 'Active') {
            res.json({
                error: 'event_is_not_active',
                error_description: 'Event is not active'
            })
            return
        }
        let attendee = getAttendee(event.created_time, code.signature)
        if (attendee === false) {
            let zone = getZone(event.created_time, zone_)
            return res.json({
                size: zone.size,
                count: zone.current.length,
                text: "You can't access this zone"
            })
            return
        }
        for (let i in attendee.zones) {
            let attZone = attendee.zones[i].zone
            if (attZone === 'Not Zoned' || zone_ == attZone) {
                let zone = getZone(event.created_time, zone_)

                const timestamp = new Date().getTime()
                const record = {
                    name: attendee.name,
                    mobileNo: attendee.mobileNo,
                    tagId: attendee.tagId,
                    created_time: timestamp,
                    actions: ''
                }

                let newCurrent = []
                let added = false

                // check this attendee is in zone or not, if yes remove attendee from current, if no add attendee into current
                // and add attendee to records
                for (let j in zone.current) {
                    let current = zone.current[j]
                    if (current.tagId != record.tagId) {
                        newCurrent.push(current)
                    } else {
                        added = true
                    }
                }
                if (!added) {
                    newCurrent.push(record)
                }

                zone.current = newCurrent
                record.action = added ? 'Check-out' : 'Check-in'

                if (record.action === 'Check-in') {
                    if (zone.size <= zone.current.length - 1) {
                        res.json({
                            size: zone.size,
                            count: zone.current.length - 1,
                            status: 'unauthorized',
                            text: event.topic + ' is full'
                        })
                        return
                    }
                }

                zone.records.push(record)

                updateZone(event.created_time, zone)

                // add records for attendees
                attendee.records.push({
                    created_time: timestamp,
                    event: event,
                    zone: {
                        name: zone.name,
                        created_time: zone.created_time
                    },
                    action: record.action
                })
                updateAttendee(event.created_time, attendee)

                if (record.action === 'Check-out') {
                    res.json({
                        status: 'success',
                        text: '' + attendee.name + ' is exiting',
                        size: zone.size,
                        count: zone.current.length
                    })
                } else {
                    res.json({
                        status: 'success',
                        text: 'Welcome ' + attendee.name,
                        size: zone.size,
                        count: zone.current.length
                    })
                }
                return
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})


app.post('/createZone', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const { created_time, zoneName, capacity } = req.body
            createZone(created_time, zoneName, capacity)
            const zones = getZones(created_time)
            res.json(zones)
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/getZoneLogs', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let { created_time, zone, type } = req.body
            zone = getZone(created_time, zone)
            const data = zone[type]
            res.json(data)
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/getLogsAttendee', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let { created_time, tagId } = req.body
            const attendee = getAttendee(created_time, tagId)
            const { records } = attendee
            res.json(records)
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/changeAttendeeMobileNo', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let { created_time, tagId, mobileNo } = req.body
            const attendee = getAttendee(created_time, tagId)
            attendee.mobileNo = mobileNo
            updateAttendee(created_time, attendee)
            delete attendee.records
            res.json(attendee)
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/editZone', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const { created_time, zone } = req.body
            editZone(created_time, zone)
            const zones = getZones(created_time)
            res.json(zones)
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/deleteZone', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const { created_time, zone } = req.body
            deleteZone(created_time, zone)
            const zones = getZones(created_time)
            res.json(zones)
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/getZones', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const { created_time } = req.body
            const event = getEvent(created_time)
            const zones = getZones(created_time,)
            for (let i in zones) {
                zones[i].count = zones[i].current.length
                zones[i].code = insert(zones[i].created_time, 3, ' ')
                zones[i].code = insert(zones[i].code, 7, ' ')
                zones[i].code = insert(zones[i].code, 11, ' ')
                delete zones[i].current
                delete zones[i].records
            }
            res.json(zones)
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/getZonesCount', async (req, res) => {
    const { created_time } = req.body
    let zones = getZones(created_time)
    zones = zones.map((zone) => {
        return {
            size: zone.size,
            count: zone.current.length,
            name: zone.name
        }
    })
    res.json(zones)
})

app.post('/deleteEvent', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const event = req.body
            deleteEvent(event.created_time)
            res.json({
                status: 'success'
            })
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/startEvent', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const { created_time } = req.body
            let event = getEvent(created_time)
            event.started_time = new Date().toISOString()
            event.status = 'Active'
            updateEvent(event)
            res.json(event)
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/closeEvent', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const { created_time } = req.body
            let event = getEvent(created_time)
            event.closed_time = new Date().toISOString()
            event.status = 'Closed'
            updateEvent(event)
            res.json(event)
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/endEvent', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const event = getEvent(req.body)
            event.ended_time = new Date().toISOString()
            event.status = 'archive'
            writeEvent(event)
            event.status = 'Active'
            deleteEvent(event)
            res.json({
                status: 'success'
            })
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/cancelEvent', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const { created_time } = req.body
            let event = getEvent(created_time)
            event.status = 'Idle'
            event.started_time = null
            updateEvent(event)
            res.json(event)
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.get('/eventStat', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const activeCount = getEvents(true)
            res.json({
                active: 0,
                available: 0,
                archive: 0
            })
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/saveEventChanged', async (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let event = req.body.event
            const sizeChanged = req.body.sizeChanged
            if (!Number.isInteger(parseInt(sizeChanged))) {
                res.json({
                    error: 'Invalid size changed value',
                    error_description: 'Invalid size changed value'
                })
                return
            }
            event = getEvent(event)
            event.size = sizeChanged
            writeEvent(event)
            res.json({
                status: 'success'
            })
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/register', (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const { tagId, zone, eventId, name, mobileNo, surname, gender, age, consents } = req.body
            let attendee = getAttendee(eventId, tagId)
            if (attendee) {
                res.json({
                    error: 'attendee_was_added',
                    error_description: 'Attendee is already added.'
                })
            } else {
                let zoneName = 'Not Zoned'
                if (zone !== 'Not Zoned') {
                    let zone_ = getZone(eventId, zone)
                    zoneName = zone_.name
                }
                createAttendee(eventId, [{ zone: zone, name: zoneName }], tagId, name, mobileNo, surname, gender, age, consents)
                attendee = getAttendee(eventId, tagId)
                attendee.zone = zoneName
                res.json(attendee)
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/getSummaryInfo', (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const { created_time } = req.body
            let attendees = getAttendees(created_time, true)
            let zones = getZones(created_time, true)
            res.json({
                attendeesCount: attendees.length,
                zonesCount: zones.length
            })
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/getAttendees', (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let { created_time } = req.body
            let attendees = getAttendees(created_time)
            res.json(attendees)
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/deleteAttendee', (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            let { created_time, tagId } = req.body
            deleteAttendee(created_time, tagId)
            const attendees = getAttendees(created_time)
            res.json(attendees)
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/realtime', (req, res) => {
    try {
        let token = '1' // for testing
        if (!token) {
            return res.json({
                error: 'Bad request',
                error_description: 'Bad Request.'
            })
        } else {
            const { selectedEvent, selectedZone } = req.body
            let event = getEvent(selectedEvent)
            if (event.status === 'Active') {
                let zone = getZone(selectedEvent, selectedZone)
                if (zone) {
                    let count = zone.current.length
                    let size = zone.size
                    res.json({
                        size: size,
                        count: count
                    })
                } else {
                    res.json({
                        error: 'Event is closed',
                        error_description: 'Event is closed'
                    })
                }
            } else {
                res.json({
                    error: 'Event is closed',
                    error_description: 'Event is closed'
                })
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.post('/realtimeNative', (req, res) => {
    const { selectedEvent, selectedZone, realtimeId } = req.body
    let event = getEvent(selectedEvent)
    if (event.status === 'Active') {
        let zone = getZone(selectedEvent, selectedZone)
        if (zone) {
            let count = zone.current.length
            let size = zone.size
            res.json({
                size: size,
                count: count
            })
        } else {
            res.json({
                error: 'this_zone_is_closed',
                error_description: 'This zone isn\' available'
            })
        }
    } else {
        res.json({
            error: 'event_is_closed',
            error_description: 'Event is closed'
        })
    }
})

app.post('/getSignatureStatus', (req, res) => {
    try {
        let token = req.headers.authorization
        if (!token) {
            let events = getEvents(true)
            if (events.length == 0) {
                res.json({
                    error: 'There is no actived event',
                    error_description: 'There is no event'
                })
            } else {

                for (let i in events) {
                    const event = events[i]
                    if (event.status !== 'Closed') continue
                    const attendees = getAttendees(event.created_time, true)
                    for (let i in attendees) {
                        if (attendees[i] === req.body.code.signature) {
                            res.json({
                                status: 'closed',
                                event: event.topic,
                                url: event.url
                            })
                            return
                        }
                    }
                }

                events = events.filter((event) => {
                    return event.status === 'Active'
                })
                events = events.map((event) => {
                    return {
                        created_time: event.created_time,
                        name: event.topic
                    }
                })
                res.json({
                    status: 'client',
                    data: events
                })
            }
        } else {
            let { code, event } = req.body
            event = getEvent(event.created_time)
            if (!event) {
                res.json({
                    error: 'Event is invalid',
                    error_description: 'Event is invalid'
                })
                return
            }
            let attendee = getAttendee(code.signature)
            let zones = getZones(event.created_time, true)
            if (!attendee) {
                for (let i in zones) {
                    delete zones[i].records
                    delete zones[i].current
                }
                res.json({
                    status: 'valid',
                    zones: zones,
                    consents: event.consents,
                    ageRanges: event.ageRanges
                })
            } else {
                res.json({
                    status: 'duplicated',
                    data: attendee,
                    zones: zones
                })
            }
        }
    } catch (e) {
        console.log(e)
        return res.json({
            error: 'internal_error',
            error_description: 'Internal Error.'
        })
    }
})

app.get('/onGetActiveEvents', (req, res) => {
    let events = getEvents()
    events = events.filter((event) => {
        return event.status === 'Active'
    }).map((event) => {
        return {
            created_time: event.created_time,
            topic: event.topic
        }
    })
    res.json(events)
})

app.post('/onGetActiveZones', (req, res) => {
    const { created_time } = req.body
    let zones = getZones(created_time)
    zones = zones.map((zone) => {
        return {
            created_time: zone.created_time,
            name: zone.name
        }
    })
    console.log(zones)
    res.json(zones)
})

app.get('/', (req, res) => {
    console.log('is called')
    res.json({
        message: 'Hello NFC'
    })
})

app.listen(port, () => {
    console.log('NFC server is running on port ', port)
})


async function getUserInfo(token) {
    let result = await mysql.select(TABLE_SESSION, { token: token })
    if (result.length > 0) {
        let user = await mysql.select(TABLE_USERS, { user_id: result[0].user_id })
        if (user.length > 0) {
            return user[0]
        }
    }
    return false
}


function getEvent(created_time) {
    let eventFolderPath = DATABASE_EVENT_PATH + '/' + created_time
    if (fs.existsSync(eventFolderPath)) {
        try {
            let eventPath = eventFolderPath + '/detail.json'
            let event = fs.readFileSync(eventPath)
            event = JSON.parse(event)
            return event
        } catch (e) {
            return false
        }
    } else {
        return false
    }
}

function updateEvent(event) {
    let eventFolderPath = DATABASE_EVENT_PATH + '/' + event.created_time
    if (fs.existsSync(eventFolderPath)) {
        try {
            let eventPath = eventFolderPath + '/detail.json'
            fs.writeFileSync(eventPath, JSON.stringify(event), 'utf8')
        } catch (e) {
            return false
        }
    } else {
        return false
    }
}

function deleteEvent(created_time) {
    let eventFolderPath = DATABASE_EVENT_PATH + '/' + created_time
    rimraf.sync(eventFolderPath);
}


function getEvents(onlyCountZones) {
    let eventsPath = DATABASE_EVENT_PATH
    let dirs = fs.readdirSync(eventsPath)
    let events = []
    for (let i in dirs) {
        const eventPath = eventsPath + '/' + dirs[i]
        console.log(eventPath)
        let event = fs.readFileSync(eventPath + '/detail.json', 'utf8')
        event = JSON.parse(event)
        event.zones = []
        let zoneDirs = fs.readdirSync(eventPath + '/zones/')
        if (onlyCountZones) {
            event.zoneLength = zoneDirs.length
        } else {
            for (let j in zoneDirs) {
                let zone = fs.readFileSync(eventPath + '/zones/' + zoneDirs[j])
                zone = JSON.parse(zone)
                event.zones.push(zone)
            }
        }
        events.push(event)
    }
    return events
}


function getZones(created_time, lightWeight) {
    let eventZonesPath = DATABASE_EVENT_PATH + '/' + created_time + '/zones'
    const dirs = fs.readdirSync(eventZonesPath)
    let zones = []
    for (let i in dirs) {
        let zone = fs.readFileSync(eventZonesPath + '/' + dirs[i])
        if (lightWeight) {
            delete zone.records
        }
        zone = JSON.parse(zone)
        zones.push(zone)
    }
    return zones
}

function insert(str, index, value) {
    str = str.toString()
    return str.substr(0, index) + value + str.substr(index)
}

function createZone(created_time, name, capacity) {
    let eventZonesPath = DATABASE_EVENT_PATH + '/' + created_time + '/zones'
    const timestamp = new Date().getTime()
    let zone = {
        created_time: timestamp,
        name: name,
        size: capacity,
        current: [],
        records: []
    }
    fs.writeFileSync(eventZonesPath + '/' + timestamp + '.json', JSON.stringify(zone), 'utf8')
}

function updateZone(created_time, zone) {
    let eventZonesPath = DATABASE_EVENT_PATH + '/' + created_time + '/zones'
    fs.writeFileSync(eventZonesPath + '/' + zone.created_time + '.json', JSON.stringify(zone), 'utf8')
}

function getZone(event_created_time, created_time) {
    let eventZonePath = DATABASE_EVENT_PATH + '/' + event_created_time + '/zones/' + created_time + '.json'
    try {
        let zone = fs.readFileSync(eventZonePath, 'utf8')
        zone = JSON.parse(zone)
        return zone
    } catch (e) {
        return false
    }
}

function editZone(created_time, zone) {
    let z = getZone(created_time, zone.created_time)
    z.size = zone.capacity
    updateZone(created_time, z)
}

function deleteZone(created_time, zone) {
    let eventZonePath = DATABASE_EVENT_PATH + '/' + created_time + '/zones/' + zone.created_time + '.json'
    fs.unlinkSync(eventZonePath)
}

function getAttendees(created_time, lightWeight) {
    const attendeesPath = DATABASE_EVENT_PATH + '/' + created_time + '/attendees'
    const attendeeDirs = fs.readdirSync(attendeesPath)
    let attendees = []
    for (let i in attendeeDirs) {
        const attendeePath = attendeesPath + '/' + attendeeDirs[i]
        let attendee
        if (lightWeight) {
            attendee = attendeeDirs[i].split('.json')[0]
        } else {
            attendee = fs.readFileSync(attendeePath, 'utf8')
            attendee = JSON.parse(attendee)
        }
        attendees.push(attendee)
    }
    return attendees
}


function getAttendee(created_time, tagId) {
    const attendeePath = DATABASE_EVENT_PATH + '/' + created_time + '/attendees/' + tagId + '.json'
    try {
        let attendee = fs.readFileSync(attendeePath)
        attendee = JSON.parse(attendee)
        return attendee
    } catch (e) {
        return false
    }
}

function updateAttendee(created_time, attendee) {
    const attendeePath = DATABASE_EVENT_PATH + '/' + created_time + '/attendees/' + attendee.tagId + '.json'
    fs.writeFileSync(attendeePath, JSON.stringify(attendee), 'utf8')
}

function deleteAttendee(created_time, tagId) {
    const attendeePath = DATABASE_EVENT_PATH + '/' + created_time + '/attendees/' + tagId + '.json'
    fs.unlinkSync(attendeePath)
}

function createAttendee(created_time, zones, tagId, name, mobileNo, surname, gender, age, consents) {
    const attendee = {
        tagId: tagId,
        name: name,
        surname: surname,
        mobileNo: mobileNo,
        gender: gender,
        age: age,
        consents: consents,
        zones: zones,
        created_time: new Date().getTime(),
        records: []
    }
    const attendeesPath = DATABASE_EVENT_PATH + '/' + created_time + '/attendees'
    fs.writeFileSync(attendeesPath + '/' + tagId + '.json', JSON.stringify(attendee), 'utf8')
}


async function getAccounts(role, publicData) {
    let accounts
    if (role) {
        accounts = await mysql.select(TABLE_USERS, {
            role: role
        })
    } else {
        accounts = await mysql.select(TABLE_USERS)
    }
    if (publicData) {
        return accounts.map((account) => {
            return {
                user_id: account.user_id,
                username: account.user_name,
                role: role
            }
        })
    } else {
        return accounts
    }
}