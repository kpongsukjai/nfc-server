
const mysql = require('mysql2')
class MySql {
    constructor() {
        this.pool = mysql.createPool({
            host: 'localhost',
            user: 'root',
            password: '1234',
            database: 'nfc',
            waitForConnections: true,
            connectionLimit: 100,
            queueLimit: 0,
            port: 3306
        })
    }

    query(command, object) {
        if (object) {
            return new Promise((resolve, reject) => {
                this.pool.query(command, object, function(err, rows, fields) {
                    if (err) {
                        console.log(err)
                        resolve(false)
                    } else {
                        resolve(rows)
                    }
                })
            })
        } else {
            return new Promise((resolve, reject) => {
                this.pool.query(command, function(err, rows, fields) {
                    if (err) {
                        console.log(err)
                        resolve(false)
                    } else {
                        resolve(rows)
                    }
                })
            })
        }
    }

    insert(table, object) {
        return new Promise((resolve, reject) => {
            this.pool.query('INSERT INTO ' + table + ' SET ?', object, function(err, rows, fields) {
                if (err) {
                    console.log(err)
                    resolve(false)
                } else {
                    resolve(rows)
                }
            })
        })
    }

    delete(table, object) {
        console.log('delete', object)
        return new Promise((resolve, reject) => {
            this.pool.query('DELETE FROM ' + table + ' WHERE ?', object, function(err, rows, fields) {
                if (err) {
                    console.log(err)
                    resolve(false)
                } else {
                    console.log('rows: ', rows)
                    resolve(rows)
                }
            })
        })
    }

    update(table, object, where, where2) {
        let objects = [object, where, where2]
        return new Promise((resolve, reject) => {
            this.pool.query('UPDATE ' + table + ' SET ? WHERE ? AND ?', objects, function(err, rows, fields) {
                if (err) {
                    console.log(err)
                    resolve(false)
                } else {
                    resolve(rows)
                }
            })
        })
    }

    select(table, object, orderColumn, order) {
        let orderText = ''
        if (orderColumn && order) {
            orderText = 'ORDER BY ' + orderColumn + ' ' + order
        }
        return new Promise((resolve, reject) => {
            if (object) {
                this.pool.query('SELECT * FROM ' + table + ' WHERE ? ' + orderText, object, function(err, rows, fields) {
                    if (err) {
                        console.log(err)
                        resolve(false)
                    } else {
                        resolve(rows)
                    }
                })
            } else {
                this.pool.query('SELECT * FROM ' + table + ' ' + orderText, function(err, rows, fields) {
                    if (err) {
                        console.log(err)
                        resolve(false)
                    } else {
                        resolve(rows)
                    }
                })
            }
        })
    }
}

module.exports = new MySql()